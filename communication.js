// trida pro komunikaci se serverovou casti

export default class Communication {
    constructor(context) {
        this.context = context;
    }

    // drzi informace k vybranemu prvku v input okenku pod kategoriemi
    searchItem = {name: "", id: -1};

    fillSearchItem(name, id, cat){
        this.searchItem.name = name;
        this.searchItem.id = Number(id);
        // console.log(this.searchItem);
    }

    changeSearched(e, sup) {
        // console.log(e.target);
        e = e.target;
        d3.selectAll("#content .searchSel").nodes().forEach((el) => {if (el.attributes.class.value === "searchSel") el.attributes.class.value = "";});
        sup.fillSearchItem(e.textContent, e.attributes.ida.value, e.attributes.cat.value);
        e.attributes["class"].value = "searchSel";
        d3.select(".form_control").node().value = e.textContent;
        d3.select("#content").style("visibility", "hidden");
    }

    fillDropDown(list, data) {
        let child = list.node().firstElementChild;
        let i = 0;
        for (i; i < data.length; i++) {
            if (child) {
                d3.select(child).attr("ida", data[i].id);
                d3.select(child).attr("cat", data[i].category.id);
                d3.select(child).text(data[i].name);
                if (i + 1 < data.length)
                    child = child.nextElementSibling;
            } else {
                list.append("li")
                    .attr("ida", data[i].id)
                    .attr("cat", data[i].category.id)
                    .attr("class", "")
                    .on("click", (e) => {this.changeSearched(e, this)})
                    .text(data[i].name);
            }
        }
        if (i === data.length && child)
            while (list.node().lastElementChild !== child)
                list.node().removeChild(list.node().lastElementChild);
        if (data.length === 0)
            while (list.node().lastElementChild)
                list.node().removeChild(list.node().lastElementChild);
    }

    async fetchTags(searchFor, data) {
        let resp = await fetch(this.context.url + `/tag/byContent?search=${searchFor}`);
        data = await resp.json();
        for (const item of data) {
            if (!this.context.data.some(el => el.name === item.name))
                this.context.data.push({
                    name: item.name,
                    clsName: item.name.split(" ").join("").replaceAll(".", ""),
                    id:item.id,
                    category:{id: item.category.id, icon: item.category.icon},
                    icon: item.icon ? item.icon.path : undefined,
                    events:undefined
                });
        }
    }

    // sestaveni toho co je na serveru s ukladanim do lokalni promenne
    async searchCats() {
        const selection = d3.select("#modal_add .form_select").node();
        let selected = selection[selection.selectedIndex];
        const list = d3.select("#content");
        // console.log(selected);
        const searchFor = d3.select(".form_control").node().value;
        if (searchFor !== "") {
            d3.select("#content").style("visibility", "inherit");
            let data = this.context.data.filter(item => item.name.includes(searchFor) && (item.category.id === Number(selected.value) || Number(selected.value) === 0));
            if (!data.length) {
                await this.fetchTags(searchFor, data);
                data = this.context.data.filter(item => item.name.includes(searchFor) && (item.category.id === Number(selected.value) || Number(selected.value) === 0));
            }
            // vyuzit filter na ziskani spravnych dat
            this.fillDropDown(list, data)
        }
        else {
            d3.select("#content").style("visibility", "hidden");
            d3.selectAll("#content").each(function (d,i) {d3.select(this).attr["class"] = "";});
            this.fillSearchItem("", -1, -1);
        }
    }

    fillSelect(data) {
        const sel = d3.select("#modal_add select");
        // console.log(sel);
        for (let i = 0; i < data.length; i++) {
            sel.append("option")
                .attr("value", data[i].id)
                .attr("title", data[i]["name"])
                .attr("alt", data[i]["description"])
                .text(data[i]["name"]);
        }
    }

    async getCategories() {
        const resp = await fetch(this.context.url + "/category/all");
        const data = (await resp.json()).slice(0,10);
        // console.log(data);
        this.fillSelect(data);
    }

// uklada do databufferu eventy dane osy
    async saveToBuffer() {
        let el = this.context.data.findIndex(e => e.id === this.searchItem.id);
        let item = this.context.data[el];
        // console.log(this.searchItem);
        if (el !== -1) {
            if (item.events === undefined){
                let resp = await fetch(this.context.url + `/event/byFilterId?id=${this.searchItem.id}`);
                item.events = await resp.json();
            }

            if (item.events.length !== 0 &&
                this.context.active.find(e => e.id === this.searchItem.id) === undefined) {
                // console.log(item);
                this.context.active.push({
                    name:this.searchItem.name,
                    clsName: item.clsName,
                    id:this.searchItem.id,
                    events:item.events,
                    color: this.context.getNextFreeColor(),
                    iconPath: item.icon !== undefined ? item.icon :
                        (item.category.icon !== undefined ? item.category.icon.path : "/img/tags/black-box.png")
                });
            }
        }
        // console.log(this.context.active);
    }

    //-----------------------debugging part
    clearElement(name) {
        const events = d3.select(name);
        while (events.node().firstElementChild)
            events.node().removeChild(events.node().lastElementChild);
    }

    iterateEvents(name, data) {
        const events = d3.select("#eventsResult");
        events.append("h2").text(name + " - počet událostí:" + data.length);
        for (const item of data) {
            const list = events.append("ul");
            list.append("li").text("id: " + item["id"]);
            list.append("li").text("Nazev: " + item["name"]);
            list.append("li").text("Popis: " + item["description"]);
            list.append("li").text("Zacatek: " + item["begin"]);
            list.append("li").text("IconsPath: " + item["icon"]["path"]);
        }
        events.append("hr");
    }

    async showEventsToId() {
        // console.log(`${control.value}`);
        this.clearElement("#eventsResult");
        // console.log(text);
        // doptat se na eventy
        let resp = await fetch(this.context.url + `/event/byFilterId?id=${searchItem.id}`);
        let data = await resp.json();
        // vypsat vysledek
        this.iterateEvents(this.searchItem.name, data);
    }
    //-----------------------\debugging part
}