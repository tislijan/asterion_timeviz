import "./styles.css";
import Context from "./context.js";
import Build from "./build.js"
import Order from "./order.js"
import Communication from "./communication.js";
import dateUtils from './currentVer/DateUtils.js';

// Tento soubor obsahuje veskera zpetna volani
// nastav potrebne promenne pro vsechny casti interakci
// aktualni vyber
let selection = null;
// ovladani z klavesnice
let keys = {shift: false, ctrl:false, up: false, down: false, e: false, d:false, a:false, esc:false};
// promenna drzici veskere potrebne informace
let context = new Context();
// vypocty poradi os
let order = new Order(context);
// vystavba cele vizualizace
let build = new Build(context, order);
// komunikace se serverem
let comm = new Communication(context);
// modalni okno
let modal = build.getElement("#modal_menu");
let modalActive = false;

// nastaveni callbacku
build.setCallBack(".form_control"
    ,"keyup", (e) => {
    if (e.key !== "Escape" && e.key !== "ArrowUp" && e.key !== "ArrowDown"
        && e.key !== "ArrowLeft" && e.key !== "ArrowRight" && e.key !== "Enter")
        comm.searchCats();
});

build.setCallBack(".dropdown","keyup", dropDownKeys);
build.setCallBack("#modal_menu","keyup", (e) => {
    if (e.key === "Escape") {
        build.setStyle("#content","visibility", "hidden");
    }
})

// nastav callback pro prvni tlacitko - upravy
build.setCallBack(build.getElement(".links").node().firstElementChild,"click",() => {
    activateModal();
    showAdd();
});

// ovladani pomoci klavesnice
build.setCallBack("body", "keydown", keyboardControlDown)
build.setCallBack("body", "keyup", keyboardControlUp)
// prirad callbacky modalnimu oknu
build.setCallBack("#buttons .close","click", closeModal);
build.setCallBack("#buttons .add","click", showAdd);
build.setCallBack("#buttons .edit","click", showEdit);
build.setCallBack("#buttons .del","click", showRemove);
build.setCallBack("#del_confirm","click", deleteSelected); // context a build
build.setCallBack("#add_confirm","click", addPath); // pujde do komunikace
build.setCallBack("#edit_confirm","click", editSelected); // do komunikace a nasledne do buildu
build.setCallBack("#frequency","click", calcFRQShowPaths); // prepocitej dle dane metody - frekvencni
build.setCallBack("#force","click", calcFMShowPaths); // prepocitej dle dane metody - silova
build.setCallBack("#random_order","click", getRandomOrderShowPaths); // prepocitej dle dane metody - nahodne poradi
build.setCallBack("#add_example", "click", autofill); // samo plnici metoda pro ukazku

// zmena vyberu zmene vyberu v modal okne
// build.getElement(".form_select").selectAll("option").each(modalSelection);

function orderCbs() {
    showPaths();
    build.getAllElements("#emblems .edit").nodes().forEach((el) => build.setCallBack(el,"click", (item) => {
        let tgt = item.target.parentNode.className.baseVal;
        build.getElement(`#emblems .${tgt} .item`).dispatch("click");
        activateModal();
        showEdit();
    }));
    build.getAllElements("#emblems .item").nodes().forEach((el) => build.setCallBack(el,"click", selectItem));
    build.getAllElements("#emblems .up").nodes().forEach((el) => build.setCallBack(el,"click", moveUp));
    build.getAllElements("#emblems .down").nodes().forEach((el) => build.setCallBack(el,"click", moveDown));
}

async function addPath() {
    // nactu z repre co chci pridat
    await comm.saveToBuffer();
    build.modalSelectAddOption();
    build.getElement(".form_select").selectAll("option").each(modalSelection);
    // necham spocitat poradi a necham zobrazit
    orderCbs();
}

function deleteSelected() {
    // zjistim co chci smazat a smazu to z active
    build.modalSelectDelOption();
    unselectItem();
    // necham spocitat poradi a necham zobrazit
    if (context.active.length > 0)
        orderCbs();
    build.resetSelection();
}

function editSelected() {
    if (build.getElement("#modal_select .form_select").node().selectedIndex !== 0){
    // zjistim druh zmeny
        if (selection.className.baseVal === build.getElement(".dropdown .form_control").node().value ||
            build.getElement(".dropdown .form_control").node().value === ""){
            let col = build.getElement("#edit_color").node().value;
            context.changeColor(build.getElement(`#paths .${selection.className.baseVal}`).attr("stroke"), col);
            build.getAllElements(`.minimapDraw .${selection.className.baseVal}`).nodes().forEach((d) => {build.getElement(d).attr("fill", col)});
            build.getElement(`#paths .${selection.className.baseVal}`).attr("stroke", col);
            let idx = context.active.findIndex(d => d.name === selection.className.baseVal);
            context.active[idx].color = col;
        } else {
    // zmena osy, vymazu tu kterou jsem vybral
            build.modalSelectDelOption();
            unselectItem();
    //      pridam tu kterou chci
            addPath();
        }
        showEdit();
    }
}

function changeModalSelection() {
    if (selection) {
        const clrPicker = build.getElement("#edit_color").node();
        const clsName = selection.className.baseVal;
        let col = build.getElement(`#paths .${clsName}`).attr("stroke");
        clrPicker.value = col;
        build.getElement("#modal_select img").attr("src", `${build.getElement(`#emblems .${clsName} .item`).attr("href")}`);
        let form = build.getElement(".form_select").node();
        for (let opt of form.options){
            if (opt.className.includes(clsName)) {
                form.selectedIndex = opt.index;
                break;
            }
        }
    } else {
        build.getElement(".form_select").node().selectedIndex = 0;
        build.getElement("#modal_select img").attr("src", require(`./img/eternity.png`));
    }
}

// ovladani modalniho okna s moznostmi na pridani, vymazani, zmenu
function activateModal() {
    modalActive = true;
    modal.style("display","inherit");
}

function closeModal() {
    build.getElement(".dropdown .form_control").node().value = "";
    modalActive = false;
    modal.style("display","none");
}

// prepinani modalniho okna editacni polozka
function showEdit() {
    build.getElement("#buttons .add").node().classList.remove("selected_tab");
    build.getElement("#buttons .del").node().classList.remove("selected_tab");
    build.getElement("#buttons .edit").node().classList.add("selected_tab");
    build.setStyle("#modal_select", "visibility", "inherit");
    build.setStyle("#del_confirm","visibility", "hidden");
    build.setStyle("#modal_add","visibility", "inherit");
    build.setStyle("#add_confirm","visibility", "hidden");
    build.setStyle("#add_example","visibility", "hidden");
    build.setStyle("#modal_edit","visibility", "inherit");
    build.setStyle("#edit_confirm","visibility", "inherit");
    build.setStyle("#change_order","visibility", "inherit");
    build.getElement(".dropdown .form_control").node().value = "";
    changeModalSelection();
}
// polozka na pridavani
function showAdd() {
    // build.getElement("#buttons .edit").classList.remove("selected_tab");
    build.getElement("#buttons .add").node().classList.add("selected_tab");
    build.getElement("#buttons .del").node().classList.remove("selected_tab");
    build.getElement("#buttons .edit").node().classList.remove("selected_tab");
    build.setStyle("#modal_select","visibility", "hidden");
    build.setStyle("#del_confirm","visibility", "hidden");
    build.setStyle("#modal_add","visibility", "inherit");
    build.setStyle("#add_confirm","visibility", "inherit");
    build.setStyle("#add_example","visibility", "inherit");
    build.setStyle("#modal_edit","visibility", "hidden");
    build.setStyle("#change_order","visibility", "visible");
    build.setStyle("#edit_confirm","visibility", "hidden");
}
// polozka na odstraneni
function showRemove() {
    build.getElement("#buttons .add").node().classList.remove("selected_tab");
    build.getElement("#buttons .del").node().classList.add("selected_tab");
    build.getElement("#buttons .edit").node().classList.remove("selected_tab");
    build.setStyle("#modal_select","visibility", "inherit");
    build.setStyle("#del_confirm","visibility", "inherit");
    build.setStyle("#modal_add","visibility", "hidden");
    build.setStyle("#add_confirm","visibility", "hidden");
    build.setStyle("#modal_edit","visibility", "hidden");
    build.setStyle("#edit_confirm","visibility", "hidden");
    build.setStyle("#change_order","visibility", "inherit");
    changeModalSelection();
}
// brush event callback
function brushed(event) { // move
    if (event.sourceEvent && event.sourceEvent.type === "zoom") return; // ignore brush-by-zoom
    let s = event.selection || context.xt.range();
    s = s.map(value => value - build.minimapX);
    context.xt2.domain(s.map(context.xt.invert, context.xt));
    for(let child of build.getElement("#paths").node().children)
        build.getElement(child).attr("d", context.area);
    build.rescaleTimeline();
    build.getElement("#timestamps").call(context.axis);
    build.getElement("#zoom").call(context.zoom.transform,
        d3.zoomIdentity
        .scale((build.minimapW) / ((s[1]) - (s[0])))
        .translate(-s[0], 0),
        null,
        event);
}
// zoom event callback
function zoomed(event) { // zoom
    if (event.sourceEvent && event.sourceEvent.type === "brush") return;
    let t = event.transform;
    context.xt2.domain(t.rescaleX(context.xt).domain());
    for(let child of build.getElement("#paths").node().children)
        build.getElement(child).attr("d", context.area);
    if (event.sourceEvent) {
        let par = build.getElement(build.getElement("#timelines").node().parentNode);
        let y = Number(par.node().transform.baseVal.consolidate().matrix.f);
        par.attr("transform", `translate(0,${y + event.sourceEvent.movementY})`);
    }
    build.rescaleTimeline();
    build.getElement("#timestamps").call(context.axis);
    build.getElement("#gb").call(context.brush.move, context.xt.range().map(t.invertX, t).map(value => value + build.minimapX), event);
}

// vyber urcitou linku
function selectItem(event) {
    // console.log(event);
    const selIcon = build.getElement("#selected");
    if (selection === null)
        selIcon.style("display", "inherit")
    selection = build.getElement(this.parentNode).node();
    let el = build.getElement(`#modal_select .form_select .${selection.className.baseVal}`);
    el.dispatch("click");
    const nY = Number(selection.transform.baseVal.consolidate().matrix.f); // nova y souradnice
    selIcon.attr("y", `${nY - 30}`);
}
// zrus vyber
function unselectItem() {
    selection = null;
    if (selection !== null)
        build.getElement("#modal_select .form_select option").dispatch("click");
    build.setStyle("#selected","display","none");
}
// prohod dve skupiny v reprezentaci
function swapGroups(idx, otherIdx) {
    let clicked = context.activeOrder[idx];
    let other = context.activeOrder[otherIdx];
    let sel = selection ? selection.className.baseVal : undefined;
    let clkedAIdx = context.active.findIndex(e => e.clsName === clicked);
    let abvAIdx = context.active.findIndex(e => e.clsName === other);
    [context.active[clkedAIdx], context.active[abvAIdx]] = [context.active[abvAIdx], context.active[clkedAIdx]];
    [context.activeOrder[idx], context.activeOrder[otherIdx]] = [context.activeOrder[otherIdx], context.activeOrder[idx]];
    build.drawRes(order.makeView());
    if (sel === clicked)
        moveSelectionTo(clicked);
    if (sel === other)
        moveSelectionTo(other);
}

// pohnu uzlem, posunu ho o jednu v aktivnim listu, prekreslim
function moveUp() {
    let idx = context.activeOrder.findIndex(e => e === this.parentNode.className.baseVal);
    if (idx !== 0)
        swapGroups(idx, idx - 1);
}

// pohnu uzlem, posunu ho o jednu v aktivnim listu, vycistim a necham znovu zobrazit
function moveDown() {
    let idx = context.activeOrder.findIndex(e => e === this.parentNode.className.baseVal);
    if (idx !== context.active.length - 1)
        swapGroups(idx, idx + 1);
}

// pohnu s vyberem linky
function moveSelectionTo(clss) {
    if (selection !== null)
        build.getElement(`#emblems .${clss} .item`).dispatch("click");
}

// vezme data z databufferu a zkusi je zobrazit
function showPaths () {
    context.updateScales();
    build.drawRes(order.makeView());
}

// brute force method
async function calcBFShowPaths() {
    order.bruteForceOrder(build);
}

// random order
async function getRandomOrderShowPaths() {
    let tmpCls = selection.className.baseVal;
    order.randomOrder(build);
    clickEmblemItem(tmpCls);
}

// frequency table
async function calcFRQShowPaths() {
    let tmpCls = selection.className.baseVal;
    order.frequencyTable(build);
    clickEmblemItem(tmpCls);
}

// force method
async function calcFMShowPaths() {
    let tmpCls = selection.className.baseVal;
    order.forceMethod(build);
    clickEmblemItem(tmpCls);
}

// automatické testovani
async function autofill() {
    const ids = [3,19,88,71,40,13,69,21];
    const names = ["Almendor", "Danérie", "Storabsko", "Podzemní říše", "Keledor", "Boševal", "Plavena", "Detreon"];
    for (let i = 0; i < names.length; i++) {
        comm.searchItem.id = ids[i];
        comm.searchItem.name = names[i];
        await comm.fetchTags(names[i],[]);
        await addPath();
    }
    comm.searchItem.id = -1;
    comm.searchItem.name = "";
}

// vyber z modal okna
function modalSelection (d,i) {
    if (i) {
        build.setCallBack(this,"click", () => {
            let el = build.getElement(`#emblems .${this.className} .item`);
            if (el.node().parentElement !== selection)
                el.dispatch("click");
            changeModalSelection();
        });
    }
    else {
        build.setCallBack(this,"click",() => {build.getElement("#modal_select img").attr("src", require("./img/eternity.png"));
            unselectItem();
        });
    }
}

function clickEmblemItem(cls) {
    build.getElement(`#emblems .${cls} .item`).dispatch("click");
}

    // ovladani klavesnice
function keyboardControlDown(e) {
    if (e.key === "Shift")
        keys.shift = true;
    if (e.key === "Control")
        keys.ctrl = true;
    if (e.key === "ArrowUp")
        keys.up = true;
    if (e.key === "ArrowDown")
        keys.down = true;
    if (e.key === "e")
        keys.e = true;
    if (e.key === "a")
        keys.a = true;
    if (e.key === "d")
        keys.d = true;
    if (e.key === "Escape")
        keys.esc = true;

    if (keys.esc && modalActive && build.getStyle("#content","visibility") === "hidden"){
        closeModal();
    }
    else if (keys.esc && selection !== null && build.getStyle("#content","visibility") === "hidden") {
        unselectItem();
    }
    if (keys.shift && keys.up && selection !== null && build.getStyle("#content","visibility") === "hidden") {
        e.preventDefault();
        build.getElement(selection).select(".up").dispatch("click");
    }
    if (keys.shift && keys.down && selection !== null && build.getStyle("#content","visibility") === "hidden") {
        e.preventDefault();
        build.getElement(selection).select(".down").dispatch("click");
    }
    if (keys.ctrl && keys.e && selection !== null){
        e.preventDefault();
        activateModal();
        showEdit();
    }
    if (keys.ctrl && keys.a) {
        e.preventDefault();
        activateModal();
        showAdd();
    }
    if (keys.ctrl && keys.d && selection !== null) {
        e.preventDefault();
        activateModal();
        showRemove();
    }
    if (keys.up && !keys.shift && selection !== null && build.getStyle("#content","visibility") === "hidden"){
        e.preventDefault();
        if (context.activeOrder.length) {
            const sel = context.activeOrder.findIndex(e => e === selection.className.baseVal) - 1;
            if (sel !== -1)
                build.getElement(`#emblems .${context.activeOrder[sel]} .item`).dispatch("click");
        }
    }
    if (keys.up && !keys.shift && selection === null && build.getStyle("#content","visibility") === "hidden"){
        e.preventDefault();
        if (context.activeOrder.length) {
            const sel = context.activeOrder.length - 1;
            build.getElement(`#emblems .${context.activeOrder[sel]} .item`).dispatch("click");
        }
    }
    if (keys.down && !keys.shift && selection !== null && build.getStyle("#content","visibility") === "hidden"){
        e.preventDefault();
        if (context.activeOrder.length) {
            const sel = context.activeOrder.findIndex(e => e === selection.className.baseVal) + 1;
            if (sel !== context.activeOrder.length)
                build.getElement(`#emblems .${context.activeOrder[sel]} .item`).dispatch("click");
        }
    }
    if (keys.down && !keys.shift && selection === null && build.getStyle("#content","visibility") === "hidden"){
        e.preventDefault();
        if (context.activeOrder.length) {
            build.getElement(`#emblems .${context.activeOrder[0]} .item`).dispatch("click");
        }
    }
}

function keyboardControlUp(e) {
    if (e.key === "Shift")
        keys.shift = false;
    if (e.key === "Control")
        keys.ctrl = false;
    if (e.key === "ArrowUp")
        keys.up = false;
    if (e.key === "ArrowDown")
        keys.down = false;
    if (e.key === "e")
        keys.e = false;
    if (e.key === "a")
        keys.a = false;
    if (e.key === "d")
        keys.d = false;
    if (e.key === "Escape")
        keys.esc = false;
}

function dropDownKeys (e){
    e.preventDefault();
    if (build.getElement(".form_control").node().value !== "" &&
        (e.key === "ArrowUp" || e.key === "ArrowDown" || e.key === "Enter")) {
        const list = build.getElement("#content").node();
        let sel = build.getElement("#content .searchSel").node();
        // console.log(sel);
        if (e.key === "ArrowUp") {
            // console.log(e.key);
            if (!sel){
                list.lastElementChild.attributes["class"].value = "searchSel";
            }
            else {
                sel.attributes["class"].value = "";
                if (sel.previousElementSibling)
                    sel.previousElementSibling.attributes["class"].value = "searchSel";
                else
                    list.lastElementChild.attributes["class"].value = "searchSel";
            }
            sel = build.getElement("#content .searchSel").node();
            sel.scrollIntoView(true);
        }
        if (e.key === "ArrowDown") {
            // console.log(e.key);
            if (!sel){
                list.firstElementChild.attributes["class"].value = "searchSel";
            }
            else {
                sel.attributes["class"].value = "";
                if (sel.nextElementSibling)
                    sel.nextElementSibling.attributes["class"].value = "searchSel";
                else
                    list.firstElementChild.attributes["class"].value = "searchSel";
            }
            sel = build.getElement("#content .searchSel").node();
            sel.scrollIntoView(true);
        }
        if (e.key === "Enter") {
            // console.log(e.key);
            build.getElement(sel).dispatch("click");
            build.setStyle("#content","visibility", "hidden");
        }
    }
}


// window.onload = (event) => {
//     console.log("hello");
    let info = build.getMinimapInfo();
    context.shuffelColors();

    context.setBrush(info, brushed);
    context.setZoom(info, zoomed);

    context.axis.tickFormat((d) => {return dateUtils.getDateName(d)});
    context.updateBrushZoomAxis(info);
    build.getElement("#timestamps").attr("transform", `translate(0,${build.getElement(".canvasDraw").node().clientHeight - 160})`);

    comm.getCategories();
// }
// console.log("here");
